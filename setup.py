from setuptools import setup, find_packages

from os import path
this_directory = path.abspath(path.dirname(__file__))
with open(path.join(this_directory, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='nautical-cache',
    version='0.0.1',
    license='MIT',
    long_description=long_description,
    long_description_content_type='text/markdown',
    packages=find_packages(),
    package_data={'': ['*.txt']},
    python_requires='>=3.6, <4',
    install_requires=[
        'nautical>=2.1.2'
    ],
    entry_points={
        'console_scripts': [
            'NauticalTests=nautical.tests.nautical_tests:main'
        ]
    },
    url='https://barbacbd@bitbucket.org/barbacbd/nautical_cache',
    download_url='https://barbacbd@bitbucket.org/barbacbd/nautical_cache/archive/v_001.tar.gz',
    description='Extension to the nautical package to provide a cache of data.',
    author='Brent Barbachem',
    author_email='barbacbd@dukes.jmu.edu',
    include_package_data=True,
    zip_safe=False
)
