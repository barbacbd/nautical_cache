"""
Author: barbacbd
Date:   5/17/2020
"""
from sys import platform
from enum import Enum


class System(Enum):
    """
    Supported Systems include Linux Distros, Windows, and MAC OSX (hopefully)
    """
    UNKNOWN = 0
    LINUX = 1
    MAC_OS = 2
    WINDOWS = 3

    @classmethod
    def python_system_name(cls,):
        """
        :return: The matching enumeration value for the python sys.platform,
                 UNKNOWN means failure or unrecognized value
        """
        return {
            "linux": cls.LINUX,
            "darwin": cls.MAC_OS,
            "win32": cls.WINDOWS
        }.get(platform.lower(), cls.UNKNOWN)
