"""
Author: barbacbd
Date:   6/16/2020
"""


class Singleton(type):

    def __init__(cls, name, bases=None, dict=None):
        """
        type(object_or_name, bases, dict)
        type(object) -> the object's type
        type(name, bases, dict) -> a new type
        """
        super(Singleton, cls).__init__(name, bases, dict)

        cls.instance = None

    def __call__(cls, *args, **kwargs):
        """
        :return: If the instance does not exist, create it. Return this instance
        """
        if not cls.instance:
            cls.instance = super(Singleton, cls).__call__(*args, **kwargs)

        return cls.instance
