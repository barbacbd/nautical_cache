"""
Author: barbacbd
Date:   6/16/2020
"""
from nautical.database.meta import Singleton

from .system import System
from getpass import getuser
from os.path import exists, dirname
from os import getenv, mkdir
from sqlite3 import connect, Error
from copy import copy
from nautical.time.enums import TimeFormat


class NauticalDatabase(metaclass=Singleton):

    def __init__(self):
        """

        """

        self._db_filename = NauticalDatabase.database_filename()

        self._db_connection = None
        self._create_connection()  # set the db connection

        if self._db_connection:
            self._create_tables()

    @staticmethod
    def database_filename():
        """
        Find the database file for your system. This function will also create the
        directory for the system if it does not exist.
        :return: Filename if successful, otherwise None
        """

        base = __name__.split(".")[0]

        directory = {
            System.LINUX: "/home/{}/.{}".format(getuser(), base),
            System.WINDOWS: "{}/{}".format(getenv('APPDATA'), base),
            System.MAC_OS: "/home/{}/Library/{}".format(getuser(), base)
        }.get(System.python_system_name(), None)

        if directory:
            if not exists(directory):
                mkdir(directory)

            return "{}/{}.db".format(directory, base)

        return None

    def _create_connection(self):
        """
        Create a database connection to the SQLite database (if the db_file does not exist, create it.)
        """
        if not exists(self._db_filename):
            # touch the file
            open(self._db_filename, 'a').close()

        if not self._db_connection:
            try:
                self._db_connection = connect(self._db_filename)
            except Error as e:
                pass

    def _create_tables(self):
        """
        Read in the tables data from the tables.txt file
        """
        c = self._db_connection.cursor()

        _f = f"{dirname(__file__)}/tables.txt"
        with open(_f, 'r') as sql_file:
            sql_file_statements = sql_file.read()

        table_statements = [f"{x};" for x in sql_file_statements.split(";") if x]

        with self._db_connection:
            try:
                for statement in table_statements:
                    c.execute(statement)
            except Error as e:
                pass

    def insert_source(self, source):
        """
        Insert all of the data that is currently filling out the source.
        :param source: Source object that contains buoys categorized by the source.
        """

        c = self._db_connection.cursor()

        with self._db_connection:
            for buoy in source:
                sql = '''INSERT INTO buoy_sources VALUES(?,?,?)'''
                buoy_source = (str(buoy.station), int(hash(buoy)), str(source.name))
                c.execute(sql, tuple(buoy_source))

                self.insert_buoy(buoy)

    def insert_buoy(self, buoy):
        """
        Insert all of the data that is currently filling out the buoy.
        :param buoy: Buoy object that contains past and present data
        """
        c = self._db_connection.cursor()

        with self._db_connection:
            if buoy.present:

                t_time = None

                fields = ['buoy']
                values = [buoy.station]
                for field_data in buoy.present:

                    if "time" in field_data:
                        t_time = copy(field_data[1])

                        # convert to 24 hour format, NO EFFECT if already in this format
                        t_time.fmt = TimeFormat.HOUR_24

                        fields.append("hour")
                        values.append(t_time.hours)
                        fields.append("minutes")
                        values.append(t_time.minutes)
                    else:
                        fields.append(field_data[0])
                        values.append(field_data[1])

                if not t_time:
                    return  # no time, no need to do anything

                find = '''SELECT * from buoys WHERE buoy=? AND year=? AND mm=? AND dd=? AND hour=? and minutes=?'''
                query = (buoy.station, buoy.present.year, buoy.present.mm, buoy.present.dd, t_time.hours, t_time.minutes)
                c.execute(find, tuple(query))

                if len(c.fetchall()) > 0:
                    sql = "UPDATE buoys SET {} WHERE buoy=? AND year=? AND mm=? AND dd=? AND hour=? and minutes=?".format(
                        ' ,'.join(["{} = ?".format(fields[i]) for i in range(len(fields))])
                    )
                    values.extend(query)
                    c.execute(sql, tuple(values))
                else:
                    sql = '''INSERT INTO buoys({}) VALUES({})'''.format(",".join(fields), ",".join(['?'] * len(values)))
                    c.execute(sql, tuple(values))

        # Let's go ahead and add in the location of this buoy whether or not the above statements took place
        self.insert_buoy_location(buoy)

    def insert_buoy_location(self, buoy):
        """
        :param buoy: buoy whos location shall be inserted if it exists
        """
        if not buoy.location:
            return

        c = self._db_connection.cursor()

        with self._db_connection:
            find = '''SELECT * from buoy_location WHERE buoy_hash=? AND latitude_deg=? AND longitude_deg=? AND altitude_m=?'''
            row = (int(hash(buoy)), buoy.location.latitude, buoy.location.longitude, buoy.location.altitude)
            c.execute(find, row)

            if len(c.fetchall()) <= 0:
                sql = '''INSERT INTO buoy_location VALUES(?,?,?,?)'''
                c.execute(sql, row)
